
package com.example.luka.zadaca1;

import android.os.Parcelable;
import android.os.Parcelable.Creator;
import org.parceler.Generated;
import org.parceler.InjectionUtil;
import org.parceler.ParcelWrapper;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2016-03-27T21:38+0200")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Scientist$$Parcelable
    implements Parcelable, ParcelWrapper<com.example.luka.zadaca1.Scientist>
{

    private com.example.luka.zadaca1.Scientist scientist$$0;
    @SuppressWarnings("UnusedDeclaration")
    public final static Scientist$$Parcelable.Creator$$0 CREATOR = new Scientist$$Parcelable.Creator$$0();

    public Scientist$$Parcelable(android.os.Parcel parcel$$0) {
        com.example.luka.zadaca1.Scientist scientist$$2;
        if (parcel$$0 .readInt() == -1) {
            scientist$$2 = null;
        } else {
            scientist$$2 = readcom_example_luka_zadaca1_Scientist(parcel$$0);
        }
        scientist$$0 = scientist$$2;
    }

    public Scientist$$Parcelable(com.example.luka.zadaca1.Scientist scientist$$4) {
        scientist$$0 = scientist$$4;
    }

    @Override
    public void writeToParcel(android.os.Parcel parcel$$1, int flags) {
        if (scientist$$0 == null) {
            parcel$$1 .writeInt(-1);
        } else {
            parcel$$1 .writeInt(1);
            writecom_example_luka_zadaca1_Scientist(scientist$$0, parcel$$1, flags);
        }
    }

    private com.example.luka.zadaca1.Scientist readcom_example_luka_zadaca1_Scientist(android.os.Parcel parcel$$2) {
        com.example.luka.zadaca1.Scientist scientist$$1;
        scientist$$1 = new com.example.luka.zadaca1.Scientist();
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "lastName", parcel$$2 .readString());
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "rank", parcel$$2 .readInt());
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "quote", parcel$$2 .readString());
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "about", parcel$$2 .readString());
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "birthDate", parcel$$2 .readString());
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "firstName", parcel$$2 .readString());
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "deathDate", parcel$$2 .readString());
        InjectionUtil.setField(com.example.luka.zadaca1.Scientist.class, scientist$$1, "imageURL", parcel$$2 .readString());
        return scientist$$1;
    }

    private void writecom_example_luka_zadaca1_Scientist(com.example.luka.zadaca1.Scientist scientist$$3, android.os.Parcel parcel$$3, int flags$$0) {
        parcel$$3 .writeString(InjectionUtil.getField(java.lang.String.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "lastName"));
        parcel$$3 .writeInt(InjectionUtil.getField(int.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "rank"));
        parcel$$3 .writeString(InjectionUtil.getField(java.lang.String.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "quote"));
        parcel$$3 .writeString(InjectionUtil.getField(java.lang.String.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "about"));
        parcel$$3 .writeString(InjectionUtil.getField(java.lang.String.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "birthDate"));
        parcel$$3 .writeString(InjectionUtil.getField(java.lang.String.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "firstName"));
        parcel$$3 .writeString(InjectionUtil.getField(java.lang.String.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "deathDate"));
        parcel$$3 .writeString(InjectionUtil.getField(java.lang.String.class, com.example.luka.zadaca1.Scientist.class, scientist$$3, "imageURL"));
    }

    @Override
    public int describeContents() {
        return  0;
    }

    @Override
    public com.example.luka.zadaca1.Scientist getParcel() {
        return scientist$$0;
    }

    public final static class Creator$$0
        implements Creator<Scientist$$Parcelable>
    {


        @Override
        public Scientist$$Parcelable createFromParcel(android.os.Parcel parcel$$4) {
            return new Scientist$$Parcelable(parcel$$4);
        }

        @Override
        public Scientist$$Parcelable[] newArray(int size) {
            return new Scientist$$Parcelable[size] ;
        }

    }

}
