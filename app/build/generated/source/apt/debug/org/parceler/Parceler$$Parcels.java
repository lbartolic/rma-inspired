
package org.parceler;

import java.util.HashMap;
import java.util.Map;
import com.example.luka.zadaca1.Scientist;
import com.example.luka.zadaca1.Scientist$$Parcelable;

@Generated(value = "org.parceler.ParcelAnnotationProcessor", date = "2016-03-27T21:38+0200")
@SuppressWarnings({
    "unchecked",
    "deprecation"
})
public class Parceler$$Parcels
    implements Repository<org.parceler.Parcels.ParcelableFactory>
{

    private final Map<Class, org.parceler.Parcels.ParcelableFactory> map$$0 = new HashMap<Class, org.parceler.Parcels.ParcelableFactory>();

    public Parceler$$Parcels() {
        map$$0 .put(Scientist.class, new Parceler$$Parcels.Scientist$$Parcelable$$0());
    }

    public Map<Class, org.parceler.Parcels.ParcelableFactory> get() {
        return map$$0;
    }

    private final static class Scientist$$Parcelable$$0
        implements org.parceler.Parcels.ParcelableFactory<Scientist>
    {


        @Override
        public Scientist$$Parcelable buildParcelable(Scientist input) {
            return new Scientist$$Parcelable(input);
        }

    }

}
