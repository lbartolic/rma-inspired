package com.example.luka.zadaca1;

import org.json.JSONArray;

/**
 * Created by luka on 21.03.16..
 */
public interface VolleyJsonArrayResponse {
    void onError(String message);
    void onResponse(JSONArray response);
}
