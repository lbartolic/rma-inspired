package com.example.luka.zadaca1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.json.JSONArray;
import org.parceler.Parcels;

import java.util.List;

public class Top3ScientistsFragment extends Fragment {
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefresh;
    private LinearLayout fragmentContainer;
    private LayoutInflater fragmentInflater;
    private Helper helper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fragment2, container, false);

        fragmentInflater = inflater;
        progressBar = (ProgressBar) v.findViewById(R.id.fProgressBar);
        progressBar.setVisibility(View.VISIBLE);

        swipeRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        fragmentContainer = (LinearLayout) v.findViewById(R.id.f2ScientistsHolder);
        helper = new Helper(getActivity());

        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                makeFragmentView();
            }
        });
        makeFragmentView();
    }

    public void makeFragmentView() {
        if (helper.checkInternetConnection()) {
            Scientist.getScientistsFromDb(getActivity().getApplicationContext(), "top3",
                    new VolleyJsonArrayResponse() {
                        @Override
                        public void onError(String message) {
                            progressBar.setVisibility(View.GONE);
                            swipeRefresh.setRefreshing(false);
                            Toast.makeText(getActivity(), "DB connection error.", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onResponse(JSONArray response) {
                            fragmentContainer.removeAllViews();
                            List<Scientist> scientistsList = Scientist.makeFromJSON(response);

                            for (final Scientist scientist : scientistsList) {
                                View v = fragmentInflater.inflate(R.layout.scientist_row, null);

                                TextView nameTextView = (TextView) v.findViewById(R.id.tvScientistName);
                                nameTextView.setText(scientist.getFirstName() + " " + scientist.getLastName());

                                TextView birthDeath = (TextView) v.findViewById(R.id.tvScientistBirthDeath);
                                birthDeath.setText(scientist.getBirthDate() + " - " + scientist.getDeathDate());

                                TextView aboutTextView = (TextView) v.findViewById(R.id.tvScientistAbout);
                                if (scientist.getAbout().length() > 65) {
                                    aboutTextView.setText(scientist.getAbout().substring(0, 65) +
                                            "...");
                                } else aboutTextView.setText(scientist.getAbout());

                                NetworkImageView niv = (NetworkImageView) v.findViewById(R.id.nivScientistImage);

                                ImageLoader mImageLoader;
                                mImageLoader = VolleySingleton.getInstance(getActivity()).getImageLoader();
                                niv.setImageUrl(scientist.getImageURL(), mImageLoader);

                                final String toastMsg = scientist.getQuote();
                                niv.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Toast.makeText(getActivity().getApplicationContext(), toastMsg, Toast
                                                .LENGTH_SHORT).show();
                                    }
                                });

                                nameTextView.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        Intent intent = new Intent(getActivity(), ScientistActivity.class);
                                        intent.putExtra("scientist", Parcels.wrap(scientist));
                                        startActivity(intent);
                                    }
                                });

                                fragmentContainer.addView(v);
                            }

                            progressBar.setVisibility(View.GONE);
                            swipeRefresh.setRefreshing(false);
                        }
                    });
        }
        else {
            progressBar.setVisibility(View.GONE);
            swipeRefresh.setRefreshing(false);
            Toast.makeText(getActivity(), "No internet connection.", Toast.LENGTH_LONG).show();
        }
    }
}
