package com.example.luka.zadaca1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.parceler.Parcels;

public class ScientistActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scientist);

        Intent intent = getIntent();
        Scientist scientist = Parcels.unwrap(intent.getParcelableExtra
                ("scientist"));

        showScientist(scientist);
    }

    private void showScientist(Scientist scientist) {
        TextView scientistAbout = (TextView) findViewById(R.id.activitySciAbout);
        scientistAbout.setText(scientist.getAbout());

        NetworkImageView niv = (NetworkImageView) findViewById(R.id.activitySciNiv);
        ImageLoader mImageLoader;
        mImageLoader = VolleySingleton.getInstance(this).getImageLoader();
        niv.setImageUrl(scientist.getImageURL(), mImageLoader);
    }
}
