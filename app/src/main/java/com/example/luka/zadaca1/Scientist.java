package com.example.luka.zadaca1;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luka on 14.03.16..
 */
@Parcel
public class Scientist {
    private String firstName;
    private String lastName;
    private String about;
    private String imageURL;
    private String quote;
    private int rank;
    private String birthDate;
    private String deathDate;

    public Scientist() {}

    public void setFirstName(String fn) {
        this.firstName = fn;
    }
    public void setLastName(String ln) {
        this.lastName = ln;
    }
    public void setAbout(String abt) {
        this.lastName = abt;
    }
    public void setImageURL(String imgURL) {
        this.imageURL = imgURL;
    }
    public void setQuote(String q) {
        this.quote = q;
    }
    public void setRank(int r) {
        this.rank = r;
    }

    public String getFirstName() {
        return this.firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public String getAbout() {
        return this.about;
    }
    public String getImageURL() {
        return this.imageURL;
    }
    public String getQuote() {
        return this.quote;
    }
    public int getRank() {
        return this.rank;
    }
    public String getBirthDate() {
        return this.birthDate;
    }
    public String getDeathDate() {
        return this.deathDate;
    }

    public static List<Scientist> makeFromJSON(JSONArray scientistsJSON) {
        List<Scientist> list = new ArrayList<>();
        for (int i = 0; i < scientistsJSON.length(); i++) {
            try {
                Scientist scientist = new Scientist();
                JSONObject row = scientistsJSON.getJSONObject(i);
                scientist.firstName = row.getString("first_name");
                scientist.lastName = row.getString("last_name");
                scientist.about = row.getString("about");
                scientist.imageURL = row.getString("image");
                scientist.quote = row.getString("quotes");
                scientist.rank = row.getInt("rank");
                scientist.birthDate = row.getString("birth_date");
                scientist.deathDate = row.getString("death_date");

                Log.i("Sci JSON", scientist.firstName);
                Log.i("Sci JSON", scientist.lastName);
                Log.i("Sci JSON", scientist.about);
                Log.i("Sci JSON", "" + scientist.rank);
                Log.i("Sci JSON", scientist.birthDate);
                Log.i("Sci JSON", scientist.deathDate);

                list.add(scientist);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static void getScientistsFromDb(Context context, String option, final
            VolleyJsonArrayResponse listener) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://www.worldgenetics.com/luka/android/db.php?option=" + option;
        JsonArrayRequest jsonArrReq = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onError(error.toString());
                    }
                });
        queue.add(jsonArrReq);
    }

}
